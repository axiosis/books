Книги та монографії

1. ISBN 978-1-62540-038-3 <a href="https://n2o.dev/books/n2o.pdf">Феб-фреймворк для оптимізованих підприємств 2013</a>
2. ISBN 978-1-62540-052-9 <a href="https://n2o.dev/books/bpe.pdf">Система управління процесами ПриватБанк 2015</a>
3. ISBN 978-617-8027-10-0 <a href="https://formal.uno/monography.pdf">Перша формальна система 2021</a> 
4. ISBN 978-617-8027-23-0 <a href="https://formal.uno/tex/books/erp/erp.pdf">Перша державна система 2022</a>
5. ISBN 978-617-8027-27-8 <a href="https://longchenpa.gitlab.io/top/texts/top.pdf">Кваліфікація програміста 2021</a>
6. ISBN 978-617-8027-07-0 <a href="https://longchenpa.gitlab.io/azov/texts/idea.pdf">Ідея Нації 2022</a>
